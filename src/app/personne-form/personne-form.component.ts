import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Personne } from '../personne/personne.interface';
import { PersonneService } from '../personne/personne.service';

@Component({
  selector: 'personne-form',
  templateUrl: './personne-form.component.html',
  styleUrls: ['./personne-form.component.css']
})
export class PersonneFormComponent implements OnInit {

  personne: Personne = {
    id:0,
    nom:"",
    prenom:"",
    type:"",
    filiere_id:0
   }


  constructor(private personneService: PersonneService) { }

  ngOnInit(): void {
  }
  handleSubmit() {
    console.log("Submit Form...");
    
    this.personneService
      .postPersonne(this.personne)
      .subscribe()
  }

}
