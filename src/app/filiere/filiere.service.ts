import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Filiere } from './filiere.interface';

@Injectable({
  providedIn: 'root'
})
export class FiliereService {

 
  private API:string = 'http://localhost:8082/api/filiere'

  constructor(private http: HttpClient) { }

  //methode afficher tout
getFiliere():Observable<Filiere[]>{

  return this.http.get<Filiere[]>('http://localhost:8082/api/filiere/');

}

postFiliere(filiere:Filiere):Observable<Filiere[]>{
  return this.http.post<Filiere[]>(this.API,filiere);
}

deleteFiliere(id:number):any{
  return this.http.delete<any>(this.API+'/'+id);
}

updateFiliere(filiere: Filiere):Observable<Filiere[]>{
  return this.http.put<Filiere[]>(this.API, filiere)
}


}//
