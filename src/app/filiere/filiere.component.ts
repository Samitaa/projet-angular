import { Component, OnInit } from '@angular/core';
import { Filiere } from './filiere.interface';
import { FiliereService } from './filiere.service';

@Component({
  selector: 'filiere',
  templateUrl: './filiere.component.html',
  styleUrls: ['./filiere.component.css']
})
export class FiliereComponent implements OnInit {

  filieres:Filiere[] =[];

  constructor(private filiereService:FiliereService) { 
    const source$ = this.filiereService.getFiliere();
    source$.subscribe(
      //res => console.log(res)
      (res:Filiere[]) => this.filieres = res
      );
      
      
  }
  

  ngOnInit(): void {
  }

}
