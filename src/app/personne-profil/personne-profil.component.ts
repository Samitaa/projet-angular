import { Component, OnInit } from '@angular/core';
import { Personne } from '../personne/personne.interface';
import { PersonneService } from '../personne/personne.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'personne-profil',
  templateUrl: './personne-profil.component.html',
  styleUrls: ['./personne-profil.component.css']
})
export class PersonneProfilComponent implements OnInit {

  urlParam:any ="";
  constructor(private route: ActivatedRoute)
  {
   /* const source$ = this.personneService.getPersonneById(this.route.snapshot.paramMap.get("arg"));
    source$.subscribe()*/
  }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get("arg")
  }

}
