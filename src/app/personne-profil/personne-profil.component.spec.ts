import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonneProfilComponent } from './personne-profil.component';

describe('PersonneProfilComponent', () => {
  let component: PersonneProfilComponent;
  let fixture: ComponentFixture<PersonneProfilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonneProfilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonneProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
