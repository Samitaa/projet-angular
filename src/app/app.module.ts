import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { FiliereComponent } from './filiere/filiere.component';
import { FiliereFormComponent } from './filiere-form/filiere-form.component';
import { MenuComponent } from './menu/menu.component';
import { PersonneComponent } from './personne/personne.component';
import { PersonneFormComponent } from './personne-form/personne-form.component';
import { FiliereDeleteComponent } from './filiere-delete/filiere-delete.component';
import { FiliereUpdateComponent } from './filiere-update/filiere-update.component';
import { PageInfoComponent } from './filiere-page-info/page-info.component';


@NgModule({
  declarations: [
    AppComponent,
    FiliereComponent,
    FiliereFormComponent,
    MenuComponent,
    PersonneComponent,
    PersonneFormComponent,
    FiliereDeleteComponent,
    FiliereUpdateComponent,
    PageInfoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
