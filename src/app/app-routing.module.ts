import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FiliereFormComponent } from './filiere-form/filiere-form.component';
import { FiliereComponent } from './filiere/filiere.component';
import {PersonneComponent} from'./personne/personne.component';
import {PersonneFormComponent} from'./personne-form/personne-form.component';
import { FiliereDeleteComponent } from './filiere-delete/filiere-delete.component';
import { FiliereUpdateComponent } from './filiere-update/filiere-update.component';
import { PageInfoComponent } from './filiere-page-info/page-info.component';

const routes: Routes = [
  {path:'filiere', component:FiliereComponent},
  {path:'page-info/:arg', component:PageInfoComponent},
  {path :'filiere-delete', component:FiliereDeleteComponent},
  {path:'filiere-form', component:FiliereFormComponent},
  {path:'filiere-update', component:FiliereUpdateComponent},
  {path:'personne', component:PersonneComponent},
  {path:'personne-form', component:PersonneFormComponent},

  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
