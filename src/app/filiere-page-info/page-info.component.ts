import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-info',
  templateUrl: './page-info.component.html',
  styleUrls: ['./page-info.component.css']
})
export class PageInfoComponent implements OnInit {

  urlParam:any;
  constructor(private route : ActivatedRoute) { }

  ngOnInit(): void {
    this.urlParam = this.route.snapshot.paramMap.get('arg')
  }

}
