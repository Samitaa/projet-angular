import { Component, OnInit } from '@angular/core';
import { Filiere } from '../filiere/filiere.interface';
import { FiliereService } from '../filiere/filiere.service';


@Component({
  selector: 'filiere-form',
  templateUrl: './filiere-form.component.html',
  styleUrls: ['./filiere-form.component.css']
})
export class FiliereFormComponent implements OnInit {

    //toDo: rajouter une verification des champs de validation (id = nombre only etc)
  filiere:Filiere = {
    libelle:'',
    id:0,
    modules:[],
    stagiaires:[]
  }

   constructor(private filiereService:FiliereService ) { }

  ngOnInit(): void {
  }

  handleSubmit(){
    this.filiereService.postFiliere(this.filiere).subscribe(res => console.log(res))
  }
}
