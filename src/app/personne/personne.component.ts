import { Component, OnInit } from '@angular/core';
import { Personne } from '../personne/personne.interface';
import { PersonneService } from '../personne/personne.service';
@Component({
  selector: 'personne',
  templateUrl: './personne.component.html',
  styleUrls: ['./personne.component.css']
})
export class PersonneComponent implements OnInit {

  personnes:Personne[] =[];
 
  constructor(private personneService:PersonneService) { 
    const source$ = this.personneService.getPersonne();
    source$.subscribe(
     // res => console.log(res)
      (res:Personne[]) => this.personnes = res
      );
  }

  ngOnInit(): void {
  }
 deletePersonne(event:any)
  {
    this.personneService.deleteServicePersonne(event.target.value).subscribe();
  }

}
