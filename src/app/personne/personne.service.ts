import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Personne } from './personne.interface';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {
  private API:string = 'http://localhost:8082/api/personne'

  constructor(private http: HttpClient) { }

  //methode afficher tout
getPersonne():Observable<Personne[]>{

  return this.http.get<Personne[]>('http://localhost:8082/api/personne');

}

postPersonne(personne:Personne):Observable<Personne[]>{
  return this.http.post<Personne[]>(this.API,personne);
}
deleteServicePersonne(id:number)
{
  return this.http.delete(this.API+'/'+id)
}

}


