import { Component, OnInit } from '@angular/core';
import { Filiere } from '../filiere/filiere.interface';
import { FiliereService } from '../filiere/filiere.service';

@Component({
  selector: 'app-filiere-delete',
  templateUrl: './filiere-delete.component.html',
  styleUrls: ['./filiere-delete.component.css']
})
export class FiliereDeleteComponent implements OnInit {

  filieres:Filiere[] =[];
  
  selectedFiliereId:number=0;

  constructor(private filiereService:FiliereService) {
    const source$ = this.filiereService.getFiliere();
    source$.subscribe(
      //res => console.log(res)
      (res:Filiere[]) => this.filieres = res
      );
      
      

   }

  ngOnInit(): void {
  }

  handleClick(){
    this.filiereService.deleteFiliere(this.selectedFiliereId).subscribe();
   // console.log(this.selectedFiliereId);
    
  }
}
