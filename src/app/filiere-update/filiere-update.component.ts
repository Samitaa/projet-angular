import { Component, OnInit } from '@angular/core';
import { Filiere } from '../filiere/filiere.interface';
import { FiliereService } from '../filiere/filiere.service';

@Component({
  selector: 'filiere-update',
  templateUrl: './filiere-update.component.html',
  styleUrls: ['./filiere-update.component.css']
})
export class FiliereUpdateComponent implements OnInit {

  filiere:Filiere = {
    libelle:'',
    id:0,
    modules:[],
    stagiaires:[]

  }
  constructor(private filiereService:FiliereService) { }

  ngOnInit(): void {
  }

  handleSubmit(){
    this.filiereService.updateFiliere(this.filiere).subscribe(res => console.log(res))
  }



}
